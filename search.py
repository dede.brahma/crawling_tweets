#!/usr/bin/python3
# coding=utf-8

# Import Library yang dibutuhkan
import tweepy
import csv
import pandas as pd

# Masukkan Twitter Token API
consumer_key = ''
consumer_secret = ''
access_token = ''
access_token_secret = ''

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth,wait_on_rate_limit=True)

# Buat path file CSV kedalam variabel
csvFile = open('dataset_search.csv', 'a')

# buat variabel baru untuk membuat file csv
csvWriter = csv.writer(csvFile)

# Mulai crawling search tweet
for tweet in tweepy.Cursor(api.search,q="#jokowi",count=100,
                           lang="in",
                           since="2020-01-20").items():
    print (tweet.created_at, tweet.text)
    csvWriter.writerow([tweet.created_at, tweet.text.encode('utf-8')])